# Oniro-Storyline-Development



## Getting started

- This project aims at supporting the creation of Oniro storyline. There will be an issue board to follow-up different actions derived from Oniro Marketing Plan 2024.
- The objective is to contribute to the issues with proposals to later on consolidate them into a resource that can be accessible and help us to communicate what Oniro is and how it fits into the global OS platform landscape.

## Adding issues
- The creation of issues is open to everyone in the WG. For the new ones
    - Include in the title the target area for the resource
    - Include in the description more details to develop it
    - Tag in the issues those people you think can contribute to it
    - Set a deadline.

## Contributing to open issues
- Add your contributions as comments to the issues created
- Do not forget to tag those members that can help in the definition/refinement
- Push them to discussion in the marketing committee if needed.
